package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IMenuRepository;

@Service
public class MenuService {
  @Autowired
  IMenuRepository pIMenuRepository;

  public ArrayList<CMenu> getComboMenu() {
    ArrayList<CMenu> listMenu = new ArrayList<>();
    pIMenuRepository.findAll().forEach(listMenu::add);
    return listMenu;
  }
}
