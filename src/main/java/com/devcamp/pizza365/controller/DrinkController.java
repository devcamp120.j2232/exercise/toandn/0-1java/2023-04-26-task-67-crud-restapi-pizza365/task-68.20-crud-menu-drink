package com.devcamp.pizza365.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.repository.IDrinkRepository;
import com.devcamp.pizza365.service.DrinkService;

@RestController
@RequestMapping("/drink")
@CrossOrigin(value = "*", maxAge = -1)
public class DrinkController {
  @Autowired
  DrinkService drinkService;

  @Autowired
  IDrinkRepository pIDrinkRepository;

  // lấy danh sách drinks
  @GetMapping("/all")
  public ResponseEntity<List<CDrink>> getAllCDrinks() {
    try {
      return new ResponseEntity(drinkService.getAllCDrinks(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // lấy dữ liệu theo id
  @GetMapping("/detail/{id}")
  public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") int id) {
    try {
      CDrink drinkData = pIDrinkRepository.findById(id);
      return new ResponseEntity<>(drinkData, HttpStatus.OK);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // tạo drinks
  @PostMapping("/create")
  public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrinks) {
    try {
      pDrinks.setNgayTao(new Date());
      pDrinks.setNgayCapNhat(null);
      CDrink _drinks = pIDrinkRepository.save(pDrinks);
      return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
    } catch (Exception e) {
      System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
    }
  }

  // cập nhật drinks
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateDrink(@PathVariable("id") int id, @Valid @RequestBody CDrink pDrinks) {
    try {
      CDrink drinkData = pIDrinkRepository.findById(id);
      CDrink drink = drinkData;
      drink.setMaNuocUong(pDrinks.getMaNuocUong());
      drink.setTenNuocUong(pDrinks.getTenNuocUong());
      drink.setPrice(pDrinks.getPrice());
      drink.setGhiChu(pDrinks.getGhiChu());
      drink.setNgayCapNhat(new Date());
      try {
        return new ResponseEntity<>(pIDrinkRepository.save(drink), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Drink: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa theo id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") int id) {
    try {
      pIDrinkRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa tất cả
  @DeleteMapping("/deleteAll")
  public ResponseEntity<CDrink> deleteAllDrink() {
    try {
      pIDrinkRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}