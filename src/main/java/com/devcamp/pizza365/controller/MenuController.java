package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IMenuRepository;
import com.devcamp.pizza365.service.MenuService;

@RestController
@RequestMapping("/menu")
@CrossOrigin(value = "*", maxAge = -1)
public class MenuController {
  @Autowired
  MenuService menuService;
  @Autowired
  IMenuRepository pIMenuRepository;

  @GetMapping("/combo")
  public ResponseEntity<List<CMenu>> getComboMenu() {
    try {
      return new ResponseEntity(menuService.getComboMenu(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // lấy dữ liệu theo id
  @GetMapping("/detail/{id}")
  public ResponseEntity<CMenu> getMenuById(@PathVariable("id") long id) {
    try {
      CMenu menuData = pIMenuRepository.findById(id);
      return new ResponseEntity<>(menuData, HttpStatus.OK);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // tạo menu
  @PostMapping("/create")
  public ResponseEntity<Object> createMenu(@RequestBody CMenu pMenu) {
    try {
      CMenu _menu = pIMenuRepository.save(pMenu);
      return new ResponseEntity<>(_menu, HttpStatus.CREATED);
    } catch (Exception e) {
      System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Menu: " + e.getCause().getCause().getMessage());
    }
  }

  // cập nhật menu
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateMenu(@PathVariable("id") long id, @RequestBody CMenu pMenu) {
    try {
      CMenu menuData = pIMenuRepository.findById(id);
      CMenu menu = menuData;
      menu.setSize(pMenu.getSize());
      menu.setDuongkinh(pMenu.getDuongkinh());
      menu.setSuonnuong(pMenu.getSuonnuong());
      menu.setSalad(pMenu.getSalad());
      menu.setNuocngot(pMenu.getNuocngot());
      menu.setDongia(pMenu.getDongia());
      try {
        return new ResponseEntity<>(pIMenuRepository.save(menu), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Menu: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa theo id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<CMenu> deleteMenu(@PathVariable("id") long id) {
    try {
      pIMenuRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa tất cả
  @DeleteMapping("/deleteAll")
  public ResponseEntity<CMenu> deleteAllMenu() {
    try {
      pIMenuRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
