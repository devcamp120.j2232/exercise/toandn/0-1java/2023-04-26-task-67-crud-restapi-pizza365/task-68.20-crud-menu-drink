
--
-- Đang đổ dữ liệu cho bảng `vouchers`
--

INSERT INTO `drinks` (`id`, `ma_nuoc_uong`, `ten_nuoc_uong`, `gia_nuoc_uong`, `ghi_chu` , `ngay_tao`, `ngay_cap_nhat`) VALUES
(01, "TRATAC", 'Trà Tắc', "10000", null, "2023-04-20 20:05:36", "2023-04-20 20:05:36"),
(02, "COCA", 'Cocacola', "15000", null, "2023-04-20 20:05:36", "2023-04-20 20:05:36"),
(03, "PEPSI", 'Pepsi', "15000", null, "2023-04-20 20:05:36", "2023-04-20 20:05:36"),
(04, "LAVIE", 'Lavie', "12000", null, "2023-04-20 20:05:36", "2023-04-20 20:05:36"),
(05, "TRASUA", 'Trà sữa trân châu', "45000", null, "2023-04-20 20:05:36", "2023-04-20 20:05:36");

INSERT INTO `menu` (`id`, `size`, `duong_kinh`, `suon_nuong`, `salad` , `nuoc_ngot`, `don_gia`) VALUES
(01, "S", '4 cm', "2", "4 gram", "2", "200000"),
(02, "M", '6 cm', "4", "6 gram", "4", "300000"),
(03, "L", '8 cm', "6", "8 gram", "6", "400000");